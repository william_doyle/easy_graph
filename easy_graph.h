#include <math.h>


//	@author		William Doyle
//	@date		April 7th 2020
//	@filename 	easy_graph.h
//	@purpose	Describe easy_graph structures, globals, and functions

struct easy_graph {
//	char x_lbl [25];
//	char y_lbl [25];
//	int x_max;
//	int y_max;
//	int x_min;
//:w
	int y_min;
	void (*MATH_FUNCTION)(int x, int * y);
};

void show_easy_graph(struct easy_graph);

void save_easy_graph(struct easy_graph, const char * filename);

struct easy_graph * make_easy_graph(void (*math_fun)(int, int*));

void destroy_easy_graph(struct easy_graph *);

void BASIC_SINE_FUNCTION(int x, int * y);
