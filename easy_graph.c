


#include "easy_graph.h"
#include <stdio.h>
#include <stdlib.h>

void  BASIC_SINE_FUNCTION(int x, int * y){
#define A 2
#define K 1
#define phase_shift 1
#define vert_shift 5

	*y = A*sin(K*(x+phase_shift))+vert_shift;
}

void show_easy_graph(struct easy_graph self){
#define RANGE 26
	int plane[RANGE][RANGE*RANGE];
	for (int i = 0; i < RANGE; i++)
		for (int k = 0; k < RANGE; k++)
			plane[i][k] = 0;

	for (int x = 0; x < RANGE; x++){
		int y;
		self.MATH_FUNCTION(x, &y);
		plane[RANGE - (y+1)][x] = 1;
	}
	
	for (int i = 0; i < RANGE; i++){
		printf("%d\t", RANGE-(i+1));
		for (int k = 0; k < RANGE; k++){
			printf("%c ", (plane[i][k])? 'O':'_'); 
		}
		printf("\n");
	}	
	printf("\n\t");
	for (int i = 0; i < RANGE; i++){
		if (i >= 10){
			printf("%d", i);
			continue;
		}	
		printf("%d ", i);
	}
	printf("\n");

}

void save_easy_graph(struct easy_graph self, const char * filename){


}

struct easy_graph * make_easy_graph(void(*math_fun)(int, int*)){
	struct easy_graph * self = malloc(sizeof (struct easy_graph));
	if (self == NULL){
		fprintf(stderr, "Malloc failed in %s!\n", __func__);
		exit(EXIT_FAILURE);
	}
	self->MATH_FUNCTION = math_fun;
}

void destroy_easy_graph(struct easy_graph * self){
	if (self != NULL){
		free(self);
	} else {
		fprintf(stderr, "Blocked attempt to free NULL in function: %s \n", __func__);
	}
}
