# easy_graph

A tool to allow a C programmer to create graphs with a very small main program. The graphs may be shown with text (no ncurses) and saved as a png file.
