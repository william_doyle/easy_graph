#include "easy_graph.h"
#include <stdio.h>

void BASIC_SQUARE_FUNCTION(int x, int * y){
	*y = (x * x);
}

int main(){

	printf("Basic Square Easy Graph Demo, William Doyle 2020\n");
	struct easy_graph * sqrGraph = make_easy_graph(BASIC_SQUARE_FUNCTION);
	
	show_easy_graph(*sqrGraph);
	save_easy_graph(*sqrGraph, "sqr_graph.png");

	destroy_easy_graph(sqrGraph);
	return 0;
};
