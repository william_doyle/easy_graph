#include "easy_graph.h"
#include <stdio.h>

int main(){

	printf("Basic Sine Easy Graph Demo, William Doyle 2020\n");
	struct easy_graph * sinGraph = make_easy_graph(BASIC_SINE_FUNCTION);
	
	show_easy_graph(*sinGraph);
	save_easy_graph(*sinGraph, "sine_graph.png");

	destroy_easy_graph(sinGraph);
	return 1;//EXIT_SUCCESS;
};
